import static org.assertj.core.api.Assertions.assertThat;

import VecExtensions_Paolo.SystemInLog;
import VecExtensions_Paolo.SystemOutLogExt;
import VecExtensions_Paolo.SystemOutputLog;
import java.util.Scanner;
import org.junit.jupiter.api.Test;

public class testSystem {

  @Test
  @SystemInLog("Ciao")
  public void testSystemInLog() {

    // SETUP

    Scanner scanner = new Scanner(System.in);

    // EXECUTE

    String stringa = scanner.nextLine();

    // ASSERT

    assertThat(stringa).isEqualTo("Ciao");
  }

  @Test
  @SystemOutputLog()
  public void testSystemOutLog(SystemOutLogExt.StdOutLog output) {

    // SETUP

    // EXECUTE

    System.out.print("Ciao output");

    // ASSERT

    assertThat(output.getStdOutLog()).isEqualTo("Ciao output");
  }
}
